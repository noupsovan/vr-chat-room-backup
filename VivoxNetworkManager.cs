﻿using System.Collections;
using UnityEngine;
using Mirror;
using System.Linq;
using VivoxUnity;

public class VivoxNetworkManager : NetworkManager
{
    private VivoxVoiceManager m_vivoxVoiceManager;
    public string PositionalChannelName { get; private set; }
    public string LobbyChannelName = "lobbyChannel";
    public string HostingIp
    {
        get
        {
            if (NetworkServer.active)
            {
                return GetLocalIPAddress();
            }
            // You are hosting so we need to get your ip address
            if (NetworkClient.serverIp == "localhost")
            {
                return GetLocalIPAddress();
            }
            // Your connected to a remote host. Let's get their ip
            return NetworkClient.serverIp;
        }
    }

    public override void Awake()
    {
        base.Awake();
        m_vivoxVoiceManager = VivoxVoiceManager.Instance;
    }

    public void SendLobbyUpdate(VivoxVoiceManager.MatchStatus status)
    {
        var lobbyChannelId = m_vivoxVoiceManager.ActiveChannels.FirstOrDefault(ac => ac.Channel.Name == LobbyChannelName).Key;
        if (lobbyChannelId != null)
        {
            m_vivoxVoiceManager.SendMatchStatusMessage(status, HostingIp, lobbyChannelId);
        }
    }

    private string GetLocalIPAddress()
    {
        var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new System.Exception("No network adapters with an IPv4 address in the system!");
    }

    public override void OnServerAddPlayer(NetworkConnection conn, AddPlayerMessage extraMessage)
    {
        base.OnServerAddPlayer(conn, extraMessage);

        StartCoroutine(AddNewPlayer(conn));
    }


    private IEnumerator AddNewPlayer(NetworkConnection conn)
    {
        var player = conn.playerController.gameObject.GetComponent<TankSetup>();

        TeamManager.Instance.AssignTeam(player);

        // Wait until the player object is ready before adding them to the team roster.
        yield return new WaitUntil(() => player.m_IsReady);
        TeamManager.Instance.Players.Add(player);
    }

    public void LeaveAllChannels(bool includeLobby = true)
    {
        foreach (var channelSession in m_vivoxVoiceManager.ActiveChannels)
        {
            if (channelSession.AudioState == ConnectionState.Connected || channelSession.TextState == ConnectionState.Connected
                && (includeLobby || (includeLobby == false && channelSession.Channel.Name != LobbyChannelName)))
            {
                channelSession.Disconnect();
            }
        }
    }

    private void VivoxVoiceManager_OnParticipantAddedEvent(string username, ChannelId channel, IParticipant participant)
    {
        if (channel.Name == PositionalChannelName && participant.Account.Name == m_vivoxVoiceManager.LoginSession.Key.Name)
        {
            StartCoroutine(AwaitLobbyRejoin());
        }
    }

    private IEnumerator AwaitLobbyRejoin()
    {
        IChannelSession lobbyChannel = m_vivoxVoiceManager.ActiveChannels.FirstOrDefault(ac => ac.Channel.Name == LobbyChannelName);
        // Lets wait until we have left the lobby channel before tyring to join it.
        yield return new WaitUntil(() => lobbyChannel == null
        || (lobbyChannel.AudioState != ConnectionState.Connected && lobbyChannel.TextState != ConnectionState.Connected));

        m_vivoxVoiceManager.JoinChannel(LobbyChannelName, ChannelType.NonPositional, VivoxVoiceManager.ChatCapability.TextOnly, TransmitPolicy.No);
    }

    public override void OnStartClient()
    {
        Debug.Log("Starting client");

        PositionalChannelName = "Positional" + HostingIp;
        m_vivoxVoiceManager.OnParticipantAddedEvent += VivoxVoiceManager_OnParticipantAddedEvent;
        base.OnStartClient();
    }
    public override void OnStopClient()
    {
        Debug.Log("Stopping client");
        m_vivoxVoiceManager.OnParticipantAddedEvent -= VivoxVoiceManager_OnParticipantAddedEvent;
        LeaveAllChannels(false);
        base.OnStopClient();
    }
}
