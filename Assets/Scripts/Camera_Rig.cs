﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Rig : MonoBehaviour
{
    public Transform playerGlobal;
    public Transform playerLocal;
    void Update()
    {
        // playerGlobal = GameObject.Find("TeleportRig").transform;
        playerGlobal = GameObject.Find("OVRCameraRig").transform;
        playerLocal = playerGlobal.Find("TrackingSpace/CenterEyeAnchor");

        this.transform.SetParent(playerLocal);
        this.transform.localPosition = Vector3.zero;
    }
}